name:Simp1e Catppuccin Frappe

# Shadow: Crust
shadow:232634
shadow_opacity:0.35

# Border: Text
cursor_border:c6d0f5

# Normal background: Base
default_cursor_bg:303446
hand_bg:303446

# Sky
question_mark_bg:99d1db
# Colorful backgrounds' foreground: Surface0
question_mark_fg:414559

# Green
plus_bg:a6d189
plus_fg:414559

# Mauve
link_bg:ca9ee6
link_fg:414559

# Yellow
move_bg:e5c890
move_fg:414559

# Blue
context_menu_bg:8caaee
context_menu_fg:414559

forbidden_bg:414559
# Red
forbidden_fg:e78284

magnifier_bg:303446
magnifier_fg:c6d0f5

skull_bg:303446
skull_eye:c6d0f5

spinner_bg:303446
spinner_fg1:c6d0f5
spinner_fg2:c6d0f5
